export interface GhRepo {
  owner: {
    avatar_url: string;
    login: string;
  };

  description: string;
  forks_count: number;
  full_name: string;
  html_url: string;
  language: string;
  open_issues_count: number;
  stargazers_count: number;
}

