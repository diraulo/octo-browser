import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { MockBackend } from '@angular/http/testing';

import {
  BaseRequestOptions,
  ConnectionBackend,
  Http,
} from '@angular/http';

import { HomeComponent } from './../home/home.component';
import { IssueCardComponent } from './../issue-card/issue-card.component';
import { RepoComponent } from './../repo/repo.component';
import { RepoDetailsComponent } from './repo-details.component';

import { AppRoutingModule } from '../app-routing.module';

import { GithubService } from '../services/github.service';
import { APP_BASE_HREF } from '@angular/common';
import { By } from '@angular/platform-browser';
import { GhRepo } from '../gh-repo';
import { Observable } from 'rxjs';

describe('RepoDetailsComponent', () => {
  let component: RepoDetailsComponent;
  let fixture: ComponentFixture<RepoDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        IssueCardComponent,
        RepoComponent,
        RepoDetailsComponent,
      ],
      imports: [
        AppRoutingModule,
        ChartsModule,
        ReactiveFormsModule,
      ],
      providers: [
        BaseRequestOptions,
        GithubService,
        MockBackend,
        { provide: 'ghApi', useValue: 'https://api.github.com' },
        { provide: APP_BASE_HREF, useValue: '/' },
        {
          provide: Http,
          deps: [MockBackend, BaseRequestOptions],
          useFactory: (
            backend: ConnectionBackend,
            defaultOptions: BaseRequestOptions
          ) => new Http(backend, defaultOptions)
        }
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepoDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('requests all issues when user clicks on "All"', () => {
    spyOn(component, 'allIssues');

    const allFilter = fixture.debugElement.query(By.css('.all'));
    allFilter.triggerEventHandler('click', null);
    expect(component.allIssues).toHaveBeenCalled();
  });

  it('requests open issues when user clicks on "Open"', () => {
    spyOn(component, 'openIssues');

    const openFilter = fixture.debugElement.query(By.css('.open'));
    openFilter.triggerEventHandler('click', null);
    expect(component.openIssues).toHaveBeenCalled();
  });

  it('requests closed issues when user clicks on "Closed"', () => {
    spyOn(component, 'closedIssues');

    const closedFilter = fixture.debugElement.query(By.css('.closed'));
    closedFilter.triggerEventHandler('click', null);
    expect(component.closedIssues).toHaveBeenCalled();
  });
});
