import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { SpinnerVisibilityService } from 'ng-http-loader';

import { GithubService } from '../services/github.service';
import { GhRepo } from './../gh-repo';

@Component({
  selector: 'gh-app-repo-details',
  templateUrl: './repo-details.component.html',
  styleUrls: ['./repo-details.component.css']
})
export class RepoDetailsComponent implements OnInit {
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;

  repo: GhRepo;
  issues: any[];
  username: string;
  reponame: string;

  filter: any = { all: true, open: false, closed: false };

  chartLabels: string[] = ['Open', 'Closed'];
  chartColors: any[] = [{ backgroundColor: ['#00d1b2', '#ff3860'] }];
  issuesCount: number[] = [0, 0];
  chartType = 'pie';

  totalIssuesCount: number;
  openIssuesCount: number;
  closedIssuesCount: number;

  constructor(
    private route: ActivatedRoute,
    private ghService: GithubService,
    private location: Location,
    private spinner: SpinnerVisibilityService
  ) { }

  ngOnInit() {
    this.username = this.route.snapshot.paramMap.get('username');
    this.reponame = this.route.snapshot.paramMap.get('reponame');

    this.getRepo();
    this.allIssues();
  }

  getRepo(): void {
    this.ghService
      .getRepo(this.username, this.reponame)
      .subscribe(repo => {
        this.repo = repo;
        this.openIssuesCount = repo.open_issues_count;
      });
  }

  allIssues() {
    this.setFilter(true, false, false);
    this.getIssues();
  }

  openIssues() {
    this.setFilter(false, true, false);
    this.getIssues('open');
  }

  closedIssues() {
    this.setFilter(false, false, true);
    this.getIssues('closed');
  }

  goBack(): void {
    this.location.back();
  }

  private getIssues(state?: string): void {
    this.spinner.show();
    this.ghService
      .searchIssues(this.username, this.reponame, state)
      .subscribe(({ total_count, items }) => {
        this.issues = items;

        if (!state) {
          this.totalIssuesCount = total_count;
          this.setIssuesCounts();
        }

        this.spinner.hide();
      });
  }

  private setFilter(all, open, closed) {
    this.filter = { all, open, closed };
  }

  private setIssuesCounts(): void {
    this.closedIssuesCount = this.openIssuesCount && this.totalIssuesCount
      ? this.totalIssuesCount - this.openIssuesCount
      : 0;

    this.issuesCount = [this.openIssuesCount, this.closedIssuesCount];
    this.chart.chart.update();
  }
}
