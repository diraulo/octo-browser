import { Component } from '@angular/core';

@Component({
  selector: 'gh-app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor() { }
}
