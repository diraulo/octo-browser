import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';

import {
  BaseRequestOptions,
  ConnectionBackend,
  Http,
} from '@angular/http';

import { ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { HomeComponent } from './home.component';
import { IssueCardComponent } from '../issue-card/issue-card.component';
import { RepoComponent } from '../repo/repo.component';
import { RepoDetailsComponent } from '../repo-details/repo-details.component';

import { AppRoutingModule } from '../app-routing.module';

import { APP_BASE_HREF } from '@angular/common';
import { GithubService } from '../services/github.service';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        IssueCardComponent,
        RepoComponent,
        RepoDetailsComponent,
      ],
      imports: [
        AppRoutingModule,
        ChartsModule,
        ReactiveFormsModule,
      ],
      providers: [
        BaseRequestOptions,
        GithubService,
        MockBackend,
        { provide: 'ghApi', useValue: 'https://api.github.com' },
        { provide: APP_BASE_HREF, useValue: '/' },
        {
          provide: Http,
          deps: [MockBackend, BaseRequestOptions],
          useFactory: (
            backend: ConnectionBackend,
            defaultOptions: BaseRequestOptions
          ) => new Http(backend, defaultOptions)
        }
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
