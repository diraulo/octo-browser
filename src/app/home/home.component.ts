import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { FormControl } from '@angular/forms';

import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

import { SpinnerVisibilityService } from 'ng-http-loader';

import { GithubService } from '../services/github.service';
import { GhRepo } from '../gh-repo';

@Component({
  selector: 'gh-app-home',
  templateUrl: './home.component.html',
  styleUrls: []
})
export class HomeComponent implements OnInit, OnDestroy {
  searchInput: FormControl;

  repositories: Array<GhRepo>;
  term: string;

  constructor(
    private ghService: GithubService,
    private spinner: SpinnerVisibilityService
  ) { }

  ngOnInit() {
    this.searchInput = new FormControl();
    this.term = sessionStorage.getItem('currentSearchTerm');

    if (this.term) {
      this.searchInput.setValue(this.term);
      this.searchRepo();
    }

    this.searchInput.valueChanges
      .pipe(
        debounceTime(800),
        distinctUntilChanged()
      )
      .subscribe(term => {
        this.term = term;
        this.searchRepo();
      });
  }

  ngOnDestroy() {
    sessionStorage.setItem('currentSearchTerm', this.term);
  }

  searchRepo(): void {
    if (this.term !== '') {
      this.spinner.show();
      this.ghService.searchRepo(this.term)
        .subscribe(repos => {
          this.repositories = repos;
          this.spinner.hide();
        });
    }
  }
}
