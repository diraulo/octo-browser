import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgHttpLoaderModule } from 'ng-http-loader';

import { ChartsModule } from 'ng2-charts/ng2-charts';

import { GithubService } from './services/github.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { IssueCardComponent } from './issue-card/issue-card.component';
import { RepoComponent } from './repo/repo.component';
import { RepoDetailsComponent } from './repo-details/repo-details.component';

import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    RepoComponent,
    HomeComponent,
    RepoDetailsComponent,
    IssueCardComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    ChartsModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    NgHttpLoaderModule,
    ReactiveFormsModule,
  ],
  providers: [
    GithubService,
    { provide: 'ghApi', useValue: 'https://api.github.com' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
