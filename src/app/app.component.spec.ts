import { TestBed, async } from '@angular/core/testing';

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { IssueCardComponent } from './issue-card/issue-card.component';
import { RepoComponent } from './repo/repo.component';
import { RepoDetailsComponent } from './repo-details/repo-details.component';

import { AppRoutingModule } from './app-routing.module';

import { APP_BASE_HREF } from '@angular/common';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HomeComponent,
        IssueCardComponent,
        RepoComponent,
        RepoDetailsComponent,
      ],
      imports: [
        AppRoutingModule,
        ChartsModule,
        HttpClientModule,
        NgHttpLoaderModule,
        ReactiveFormsModule,
      ],
      providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'OctoBrowser'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const title = fixture.nativeElement.querySelector('#title');
    expect(title.textContent).toEqual('OctoBrowser');
  }));
});
