import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'gh-app-issue-card',
  templateUrl: './issue-card.component.html',
  styleUrls: ['./issue-card.component.css']
})
export class IssueCardComponent implements OnInit {
  @Input() issue: any;

  constructor() { }

  ngOnInit() { }
}
