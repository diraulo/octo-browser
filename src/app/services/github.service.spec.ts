import {
  TestBed,
  fakeAsync,
  inject,
  tick,
} from '@angular/core/testing';

import { MockBackend } from '@angular/http/testing';
import {
  BaseRequestOptions,
  ConnectionBackend,
  Http,
  Response,
  ResponseOptions,
} from '@angular/http';

import { GithubService } from './github.service';

const issuesFixture = require('./fixtures/issues.fixture.json');
const closedIssuesFixture = require('./fixtures/closed-issues.fixture.json');
const openedIssuesFixture = require('./fixtures/opened-issues.fixture.json');
const repositoriesFixture = require('./fixtures/repositories.fixture.json');
const repositoryFixture = require('./fixtures/repository.fixture.json');

const expectedUrl = (url: string, fixture: any, backend: MockBackend) => {
  backend.connections.subscribe(c => {
    expect(c.request.url).toBe(url);
    const response = new ResponseOptions({ body: JSON.stringify(fixture) });
    c.mockRespond(new Response(response));
  });
};

describe('GithubService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BaseRequestOptions,
        MockBackend,
        GithubService,
        { provide: 'ghApi', useValue: 'https://api.github.com' },
        {
          provide: Http,
          deps: [MockBackend, BaseRequestOptions],
          useFactory: (
            backend: ConnectionBackend,
            defaultOptions: BaseRequestOptions
          ) => new Http(backend, defaultOptions)
        }
      ]
    });
  });

  describe('getRepo', () => {
    it('returns a repository given a username and reponame',
      inject([GithubService, MockBackend], fakeAsync((ghService, mockBackend) => {
        let res;

        expectedUrl(
          'https://api.github.com/repos/twbs/bootstrap',
          repositoryFixture,
          mockBackend
        );

        ghService.getRepo('twbs', 'bootstrap')
          .subscribe(_res => {
            res = _res;
          });

        tick();

        expect(res.full_name).toBe('twbs/bootstrap');
      }))
    );
  });


  describe('searchRepo', () => {
    it('returns a list of repositories matching a given keyword',
      inject([GithubService, MockBackend], fakeAsync((ghService, mockBackend) => {
        let res;

        expectedUrl(
          'https://api.github.com/search/repositories?q=bootstrap',
          repositoriesFixture,
          mockBackend
        );

        ghService.searchRepo('bootstrap')
          .subscribe(_res => {
            res = _res;
          });

        tick();

        expect(res.length).toBe(2);
        expect(res[0].full_name).toBe('twbs/bootstrap');
      }))
    );
  });

  describe('searchIssues', () => {
    it('returns a list of issues for a given repository',
      inject([GithubService, MockBackend], fakeAsync((ghService, mockBackend) => {
        let res;

        expectedUrl(
          'https://api.github.com/search/issues?q=repo:twbs/bootstrap+type:issue',
          issuesFixture,
          mockBackend
        );

        ghService.searchIssues('twbs', 'bootstrap')
          .subscribe(_res => {
            res = _res;
          });

        tick();
        expect(res.total_count).toBe(4);
      }))
    );

    it('returns a list of open issues when given state of open',
      inject([GithubService, MockBackend], fakeAsync((ghService, mockBackend) => {
        let res;

        expectedUrl(
          'https://api.github.com/search/issues?q=repo:twbs/bootstrap+type:issue+state:open',
          openedIssuesFixture,
          mockBackend
        );

        ghService.searchIssues('twbs', 'bootstrap', 'open')
          .subscribe(_res => {
            res = _res;
          });

        tick();
        expect(res.total_count).toBe(3);
      }))
    );

    it('returns a list of closed issues when given state of closed',
      inject([GithubService, MockBackend], fakeAsync((ghService, mockBackend) => {
        let res;

        expectedUrl(
          'https://api.github.com/search/issues?q=repo:twbs/bootstrap+type:issue+state:closed',
          closedIssuesFixture,
          mockBackend
        );

        ghService.searchIssues('twbs', 'bootstrap', 'closed')
          .subscribe(_res => {
            res = _res;
          });

        tick();
        expect(res.total_count).toBe(1);
      }))
    );
  });
});
