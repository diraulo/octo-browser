import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import { GhRepo } from './../gh-repo';

@Injectable({
  providedIn: 'root'
})
export class GithubService {

  constructor(
    @Inject('ghApi') private apiUri,
    private http: Http
  ) { }

  getRepo(username: string, reponame: string): Observable<GhRepo> {
    return this.http
      .get(`${this.apiUri}/repos/${username}/${reponame}`)
      .pipe(
        map(response => response.json()),
        map(rawRepo => this.sanitizeRepo(rawRepo))
      );
  }

  searchRepo(searchTerm: string): Observable<GhRepo[]> {
    const url = `${this.apiUri}/search/repositories?q=${searchTerm}`;
    return this.search(url)
      .pipe(
        map(({ items }) => {
          return items.map(repo => this.sanitizeRepo(repo));
        })
      );
  }

  searchIssues(username: string, reponame: string, state = ''): Observable<any> {
    const repo: string = [username, reponame].join('/');
    const stateParam = state === '' ? '' : `+state:${state}`;

    const url = `${this.apiUri}/search/issues?q=repo:${repo}+type:issue${stateParam}`;
    return this.search(url);
  }

  private search(url: string): Observable<any> {
    return this.http
      .get(url)
      .pipe(map(response => response.json()));
  }

  private sanitizeRepo(rawRepo: any): GhRepo {
    const {
      description,
      forks_count,
      full_name,
      html_url,
      language,
      open_issues_count,
      owner,
      stargazers_count,
    } = rawRepo;

    return {
      owner: {
        avatar_url: owner.avatar_url,
        login: owner.login
      },
      description,
      forks_count,
      full_name,
      html_url,
      language,
      open_issues_count,
      stargazers_count,
    };
  }
}
