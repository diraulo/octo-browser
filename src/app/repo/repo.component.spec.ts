import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { HomeComponent } from '../home/home.component';
import { IssueCardComponent } from '../issue-card/issue-card.component';
import { RepoComponent } from './repo.component';
import { RepoDetailsComponent } from './../repo-details/repo-details.component';

import { AppRoutingModule } from '../app-routing.module';

import { GhRepo } from './../gh-repo';

import { APP_BASE_HREF } from '@angular/common';

describe('RepoComponent', () => {
  let component: RepoComponent;
  let fixture: ComponentFixture<RepoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        IssueCardComponent,
        RepoComponent,
        RepoDetailsComponent,
      ],
      imports: [
        AppRoutingModule,
        ChartsModule,
        // HttpClientModule,
        // NgHttpLoaderModule,
        ReactiveFormsModule,
      ],
      providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepoComponent);
    component = fixture.componentInstance;

    // mock the repo supplied by the parent component
    const expectedRepo: GhRepo = {
      owner: {
        avatar_url: 'https://avatars0.githubusercontent.com/u/2918581?v=4',
        login: 'twbs',
      },

      description: 'twbs repository',
      forks_count: 4,
      full_name: 'twbs/bootstrap',
      html_url: 'https://github.com/twbs/bootstrap',
      language: 'CSS',
      open_issues_count: 100,
      stargazers_count: 1223,
    };

    // simulate the parent setting the input property with that repo
    component.repo = expectedRepo;

    // trigger initial data binding
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('renders the correct information about the repository', () => {
    const repoTitle = fixture.nativeElement.querySelector('.title>a');
    const repoSubtitle = fixture.nativeElement.querySelector('.subtitle');
    const repoDescription = fixture.nativeElement.querySelector('.description');

    expect(repoTitle.textContent).toBe('twbs/bootstrap');
    expect(repoSubtitle.textContent).toBe('@twbs');
    expect(repoDescription.textContent).toBe('twbs repository');
  });
});
