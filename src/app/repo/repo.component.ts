import { Component, Input, OnInit } from '@angular/core';
import { GhRepo } from '../gh-repo';

@Component({
  selector: 'gh-app-repo',
  templateUrl: './repo.component.html'
})
export class RepoComponent implements OnInit {
  @Input() repo: GhRepo;

  constructor() { }

  ngOnInit() { }
}
