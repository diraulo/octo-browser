# OctoBrowser
![CircleCI](https://circleci.com/bb/diraulo/octo-browser/tree/master.svg?style=svg&circle-token=6911e6a7c65d3763a10bf5456d8b55b1496a2479)
[![Coverage Status](https://coveralls.io/repos/bitbucket/diraulo/octo-browser/badge.svg?branch=master)](https://coveralls.io/bitbucket/diraulo/octo-browser?branch=master)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Description

This application was created as part of the Technical Assessment for a position at [Black Swan](https://www.blackswan.com/)

## Features

Using this application a user is able to:

- Search github in order to view the available repositories for a given search term
- Select a particular repository in order to view more details of the selected repository
  (_URL, description, forks count, stargazers count, open issues count etc_)
- Link off to the actual GitHub page where the repository is located in order to view the code in the repository
- View a list of all the current issues for a repository in order to view the backlog of issues
- Filter the list of issues between STATE = ["Open" or "Closed"] in order to look through the filtered list
- View a PIE chart that displays the breakdown of issues for the repository (open vs closed) in order to visually see how well built and maintained the repository is

## Demo

[View demo](https://octobrowser.netlify.com/)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## ToDo: Improvements

- [ ] Add pagination for repositories and issues
- [ ] Prevent unnecessary http requests when filtering issues
